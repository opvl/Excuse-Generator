<?php

use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('', [
    'as'    => 'api.excuse.index',
    'uses'  => 'controller@index'
]);


Route::group(['prefix' => 'prefix'], function (){

    Route::get('', [
        'as'    => 'api.prefix.index',
        'uses'  => 'PrefixController@index'
    ]);

    Route::post('', [
        'as'    => 'api.prefix.store',
        'uses'  => 'PrefixController@store'
    ]);
});
