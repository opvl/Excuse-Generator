<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Excuse extends Model
{
    protected $fillable = [
        'prefix',
        'suffix',
        'body'
    ];

    protected $rules = [
        'prefix'    => 'required',
        'body'      => 'required',
        'suffix'    => 'required'
    ];
}
