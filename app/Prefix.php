<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Prefix extends Model
{

    use SoftDeletes;

    protected $fillable = [
        'content'
    ];

    protected $rules = [
        'content'       => 'required|min:1',
        'created_by'    => 'exists:users,id'
    ];

    public function user(){
        return \App\User::all();
    }
}
