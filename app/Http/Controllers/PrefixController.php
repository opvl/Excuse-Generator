<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Prefix;

class PrefixController extends Controller
{
    public function index(){
        $prefixs = Prefix::all();

        return $prefixs;
    }
}
